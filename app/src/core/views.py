#coding: utf-8

from django.shortcuts import render
from django.conf import settings

#app
from src.core.models import Jingles
from src.core.forms import ContatoForm

def home(request):

	jingles = Jingles.objects.all().order_by('-posted_at')
	jingle_header = jingles.filter(header=True)[0]
	jingles_mini_list = jingles.exclude(header=True)[:6]

	if request.method == 'POST':
		form = ContatoForm(request.POST,)
		if form.is_valid():
			contato_novo = form.save()

	else:
		form = ContatoForm()

	return render(request, 'index.html' , {
									'jingle_header': jingle_header,
									'jingles_mini_list':jingles_mini_list,
									'form': form,
									}
				)