#coding: utf-8
from django.db import models


class Contato(models.Model):
	mensagem = models.TextField(u'mensagem',)
	email = models.EmailField(u'email')
	nome = models.CharField(u'nome', max_length=80)


class Jingles(models.Model):
	titulo = models.CharField(u'titulo', max_length=60)
	link = models.CharField(u'link', max_length=255)
	header = models.BooleanField(u'header')
	posted_at = models.DateTimeField(u'posted_at', auto_now_add=True)

	def __unicode__(self):
		return self.link
