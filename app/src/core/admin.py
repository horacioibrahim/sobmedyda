#coding: utf-8

from django.contrib import admin

#app
from src.core.models import Contato, Jingles


class ContatoAdmin(admin.ModelAdmin):
	list_display = ('nome', 'mensagem', 'email')

class JinglesAdmin(admin.ModelAdmin):
	list_display = ('titulo', 'link', 'header')

admin.site.register(Contato, ContatoAdmin)
admin.site.register(Jingles, JinglesAdmin)

