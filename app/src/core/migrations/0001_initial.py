# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Contato'
        db.create_table(u'core_contato', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mensagem', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=80)),
        ))
        db.send_create_signal(u'core', ['Contato'])

        # Adding model 'Jingles'
        db.create_table(u'core_jingles', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('header', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'core', ['Jingles'])


    def backwards(self, orm):
        # Deleting model 'Contato'
        db.delete_table(u'core_contato')

        # Deleting model 'Jingles'
        db.delete_table(u'core_jingles')


    models = {
        u'core.contato': {
            'Meta': {'object_name': 'Contato'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mensagem': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '80'})
        },
        u'core.jingles': {
            'Meta': {'object_name': 'Jingles'},
            'header': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['core']